// import the contents of Express package to use for our application.
const express = require("express");

// mongoose is an ODM (Object Data Modeling) library for MongoDB and Node.js that manages models/schemas and allows a quick and easy way to connect to a MongoDB database.
const mongoose = require("mongoose")

// give the express() function from Express package a variable "app" so that it can be called more easily.
const app = express();

// mongoose's connect method takes our MongoDB Atlas connection string and uses it to connect to Atlas and authenticate our credentials, as well as specifics the database that our app needs to use.
// useNewUrlParser and useUnifiedTopology are both set to true as part of a newer mongoose update that allows more efficient way to connect to Atlas, since the older way is about to be deprecated (become obsolete).
mongoose.connect("mongodb+srv://admin:admin@testdatabase1.q32xg.mongodb.net/b153_tasks?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

// confirm that Atlas connection is successful.
// mongoose.connection.once("open", () => console.log("Now connected to MongoDB Atlas."));

// mongoose.connection is used to handel error or successful connection to MongoDB
let db = mongoose.connection;
// check mongoose.connection on error
// console.error.bind() will allow us to display the error of mongoose.connection both in your terminal and  in a browser console.
db.on("error", console.error.bind(console, "connection error"));
// confirmation that connection to mongoose is successful.
db.once("open", () => console.log("Now connected to MongoDB Atlas."));

// middleware - are functions/methods we use in our server/api
//  this middleware functions serve as gates it allows us do to do other tasks.
// this will allow us to handel the request body json.
app.use(express.json());
// declare a port number variable.
const port = 4000;

// create a GET route to check if Express is working.
app.get("/", (req, res) => {
    res.end("Hello, from Express");
});

/*
    mongoose schema
    - before we can create documents in our database, we first have to declare a "blueprint" or "template" of our document. this is to ensure that the content/fields of documents are uniform. gone are the days when we have to worry if we correctly spelled the fields in our documents.
    - schema acts a blueprint of our data/document.
    - it is a representation of how our documents is structured. it also determines the types of data and the expected fields/properties per document.
*/

// schema() is a constructor from mongoose that will allow us to create a new schema.
const taskSchema = new mongoose.Schema({

    /*
        - define the fields for th task document.
        - the task document should have a name and status field.
        - both fields must be strings/
    */

    name: String,
    status: String

});

/*
    mongoose model:
    - models are used to connect your api corresponding collection in your database. it is a representation of your documents.
    - models uses schemas to create objects/documents that corresponds to the schema. by default when creating the collection from your model, the collection name is pluralized.
    - mongoose.model(<nameOfCollection>, <schemaToFollow).
*/
let Task = mongoose.model("Task", taskSchema);
// mongoDB equivalent of Task model = db.tasks.

// POST route - add task documents:
app.post("/", (req, res) => {
    // check the incoming request body.
    // console.log(req.body);
    // req.body in terminal looks like:
        // {name: "Learn Node.js", status: "Pending"}

    // create a new task object/document from our model.
    let newTask = new Task({
        name: req.body.name,
        status: req.body.status
    });
    // console.log(newTask);

    // save() is a method from an object/document created by a model.
    // this method will allow us to save our document in our tasks collection.

    // then() and catch() chain.
    /*
        - then() is used to handel the result/return of a function. if the method/function properly returns a value, we can run a separate function to handel it.
        - catch() is used to catch/handel an error. if there is an error, we will handel it in a separate function aside from our result.
    */
    newTask.save()
    .then(result => res.send(result))
    .catch(error => res.send(error))
});

// GET route - get all task documents.
app.get("/tasks", (req, res) => {
    // res.send("Testing from get all tasks documents route")

    // to be able to find() or get all documents from a collection, we will use the find() method of our model.
    // MongoDB equivalent - db.tasks.find({})
    Task.find({})
    .then(result => res.send(result))
    .catch(error => res.send(error))
});

// Activity
const usersSchema = new mongoose.Schema({
    userName: String,
    password: String
});
const User = mongoose.model("User", usersSchema);
app.post("/users", (req, res) => {
    let newUser = new User({
        userName: req.body.userName,
        password: req.body.password
    });
    newUser.save()
    .then(result => res.send(result))
    .catch(error => res.send(error))
});

app.get("/users", (req, res) => {
    User.find({})
    .then(result => res.send(result))
    .catch(error => res.send(error))
});

// start the server and confirm that it is running.
app.listen(port, () => console.log(`server running at port ${port}`));